# README #

This is a Getting Started project to use DbFit

### What is this repository for? ###

* This repository includes instructions to get started using DbFit
* It uses samples for Oracle Database


### How do I get set up? ###

* Download DbFit (This guide uses version 3.2) from [Offical Site](https://github.com/dbfit/dbfit/releases/download/v3.2.0/dbfit-complete-3.2.0.zip)
* Unzip downloaded file and set environment variable `DBFIT_HOME` accordingly
* Oracle JDBC drivers (`ojdbc6.jar`) need to be download separately and must be copied to `$DBFIT_HOME/lib`
* Load Wiki service starting  Fitnesse:
```
cd $DBFIT_HOME
./startFitnesse.sh
```
* Test Fitnesse wiki service using the URL: http://localhost:8085


### How to run from the command line? ###
```
java -jar ./lib/fitnesse-20150424-standalone.jar -c "HelloWorld?test&format=text"
```
### How to use database connection file? ####
* Create a text file using this structure (file.properties):
```
service=localhost:49161
username=aportal
password=welcome1
database=xe
```
* Reference the file from the test using `ConnectUsingFile` command:
```
!|ConnectUsingFile|<file.properties>
```